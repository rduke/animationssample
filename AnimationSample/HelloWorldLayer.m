//
//  HelloWorldLayer.m
//  AnimationSample
//
//  Created by Dahiri Farid on 11/23/13.
//  Copyright Dahiri Farid 2013. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"
#import "AnimationSprite.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#pragma mark - HelloWorldLayer

@interface HelloWorldLayer ()

@property (nonatomic, retain) NSArray *animSprites;

@end

// HelloWorldLayer implementation
@implementation HelloWorldLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) )
    {
        [self generateAnimSprites];
        
	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    self.animSprites = nil;
	[super dealloc];
}

- (void)generateAnimSprites
{
    AnimationSprite* bear = [AnimationSprite animationSpiteWithPlist:@"AnimBear.plist"
                                                              sprite:@"AnimBear.png"
                                                         framesCount:6
                                                           frameName:@"bear"];
    
    AnimationSprite* fightingGirl = [AnimationSprite animationSpiteWithPlist:@"AnimGirl.plist"
                                                                      sprite:@"AnimGirl.png"
                                                                 framesCount:8
                                                                   frameName:@""];
    
    fightingGirl.sprite.position = ccp(100, 50);
    
    AnimationSprite* basketGirl = [AnimationSprite animationSpiteWithPlist:@"BasketGirl.plist"
                                                                    sprite:@"BasketGirl.png"
                                                               framesCount:6
                                                                 frameName:@""];

    basketGirl.sprite.position = ccp(150, 50);
    
    AnimationSprite* kitana = [AnimationSprite animationSpiteWithPlist:@"AnimKitana.plist"
                                                                sprite:@"AnimKitana.png"
                                                           framesCount:4
                                                             frameName:@""];
    kitana.sprite.position = ccp(200, 50);
    
    self.animSprites = @[bear, fightingGirl, basketGirl, kitana];
    
    for (AnimationSprite* animSprite in self.animSprites)
        [self addChild:animSprite.spriteSheet];
}

@end
