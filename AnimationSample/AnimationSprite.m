//
//  AnimationSprite.m
//  AnimationSample
//
//  Created by Dahiri Farid on 11/23/13.
//  Copyright (c) 2013 Dahiri Farid. All rights reserved.
//

#import "AnimationSprite.h"

@interface AnimationSprite ()

@property (nonatomic, retain, readwrite) CCSpriteBatchNode *spriteSheet;
@property (nonatomic, retain, readwrite) CCSprite *sprite;
@property (nonatomic, retain, readwrite) CCRepeatForever* spriteAction;

@end

@implementation AnimationSprite

+ (AnimationSprite *)animationSpiteWithPlist:(NSString *)plistName
                                      sprite:(NSString *)sprite
                                 framesCount:(NSUInteger)framesCount
                                   frameName:(NSString *)frameName
{
    return [AnimationSprite.alloc initWithPlist:plistName
                                         sprite:sprite
                                    framesCount:framesCount
                                      frameName:frameName].autorelease;
}


- (id)initWithPlist:(NSString *)plistName
             sprite:(NSString *)sprite
        framesCount:(NSUInteger)framesCount
          frameName:(NSString *)frameName
{
    NSParameterAssert(plistName);
    NSParameterAssert(sprite);
    NSParameterAssert(framesCount);
    NSParameterAssert(frameName);
    
    self = [super init];
    if (self)
    {
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:plistName];
		self.spriteSheet = [CCSpriteBatchNode batchNodeWithFile:sprite];
        
        NSMutableArray *walkAnimFrames = [NSMutableArray array];
        for (int i=1; i<=framesCount; i++)
        {
            [walkAnimFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"%@%d.png", frameName, i]]];
        }
        
        CCAnimation *walkAnim = [CCAnimation
                                 animationWithSpriteFrames:walkAnimFrames delay:0.1f];
        
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        self.sprite = [CCSprite spriteWithSpriteFrameName:[frameName stringByAppendingString:@"1.png"]];
        self.sprite.position = ccp(winSize.width/2, winSize.height/2);
        self.spriteAction = [CCRepeatForever actionWithAction:
                           [CCAnimate actionWithAnimation:walkAnim]];
        
        [self.sprite runAction:self.spriteAction];
        [self.spriteSheet addChild:self.sprite];
    }
    
    return self;
}


- (void)dealloc
{
    self.spriteSheet = nil;
    self.sprite = nil;
    self.spriteAction = nil;
    
    [super dealloc];
}

@end
