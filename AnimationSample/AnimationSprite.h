//
//  AnimationSprite.h
//  AnimationSample
//
//  Created by Dahiri Farid on 11/23/13.
//  Copyright (c) 2013 Dahiri Farid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnimationSprite : NSObject

@property (nonatomic, retain, readonly) CCSpriteBatchNode *spriteSheet;
@property (nonatomic, retain, readonly) CCSprite          *sprite;
@property (nonatomic, retain, readonly) CCRepeatForever   *spriteAction;

+ (AnimationSprite *)animationSpiteWithPlist:(NSString *)plistName
                                      sprite:(NSString *)sprite
                                 framesCount:(NSUInteger)framesCount
                                   frameName:(NSString *)frameName;

@end
