//
//  HelloWorldLayer.h
//  AnimationSample
//
//  Created by Dahiri Farid on 11/23/13.
//  Copyright Dahiri Farid 2013. All rights reserved.
//


#import <GameKit/GameKit.h>

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer
{
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
